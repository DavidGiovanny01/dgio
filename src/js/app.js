import 'jquery-smooth-scroll';
import 'jquery-validation';
import 'chartjs-plugin-deferred';
import Typed from 'typed.js';
import Noty from 'noty';
import Chart from 'chart.js';
import './fontawesome-all';
import './toggle-navbar.min';
import '../css/main.scss';

$(document).ready(function () {
    'use strict';

    $('header').toggleNav({
        slideEffect: true,
        speed: 300
    });

    $('nav a').on('click', ()=>{
        if($(window).width() < 767) toggleMenu();
    });

    $('.nav-toggler').on('click', () => {
        toggleMenu();
    });

    function toggleMenu() {
        $('.nav-items').toggleClass('hide');
        $('.nav-toggler').toggleClass('open');
    }

    const options = {
        strings: ['Full-Stack Web Developer', '^500 Ing. en Sistemas Computacionales', '^500 Especializado en el Stack de MEAN', '^500 Siempre en constante actualización'],
        typeSpeed: 50,
        startDelay: 1000,
        backSpeed: 20,
        backDelay: 1000,
        loop: true,
        showCursor: true,
        cursorChar: '<h2 class="cursor display-5">|</h2>',
    }

    new Typed('#full-stack', options);

    $('a').smoothScroll();

    new Chart('skills-chart', {
        type: 'bar',
        data: {
            labels: ['HTML5', 'CSS3', 'JavaScript', 'Angular 5', 'Vue.js 2', 'Ionic 3', 'Node.js', 'Express', 'Git', 'BDs SQL', 'MongoDB'],
            datasets: [{
                data: [98, 96, 92, 95, 76, 88, 91, 89, 96, 90, 93],
                backgroundColor: '#e9385f',
                hoverBackgroundColor: '#303030',
                pointHoverBackgroundColor: '#0ff'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips: {
                mode: 'x'
            },
            legend: {
                display: false
            },
            plugins: {
                deferred: {
                    yOffset: '60%',
                    delay: 500
                }
            }
        }
    });

    $('#send').click(function () {
        if (!$('#contact-form').valid()) return;
        const url = 'https://api.dgio.me/mail/send';
        // const url = 'http://localhost:3001/mail/send';
        var form = {
            name: $('#name').val(),
            mail: $('#mail').val(),
            message: $('#message').val()
        };
        $.ajax({
            type: 'POST',
            url: url,
            data: JSON.stringify(form),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }).done(() => {
            notify('El mensaje se ha enviado correctamente', 'success');
            cleanForm();
        }).fail(err => {
            if (err.responseJSON && err.responseJSON.responseCode === 503) {
                notify('No fue posible enviar el mensaje', 'warning');
            } else {
                notify('Hubo un error al enviar el mensaje', 'error');
            }
        });
    });

    function notify(text, type) {
        new Noty({
            theme: 'nest',
            layout: 'bottomCenter',
            timeout: 5000,
            text,
            type
        }).show();
    }

    function cleanForm() {
        $('#name').val('');
        $('#mail').val('');
        $('#message').val('');
        $('html, body').animate({ scrollTop: 0 }, 1000);
    }

    $('#contact-form').validate({
        rules: {
            name: {
                required: true
            },
            mail: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 15
            },
        },
        messages: {
            name: 'El nombre es requerido',
            mail: {
                required: 'El correo es requerido',
                email: 'El correo no es válido'
            },
            message: {
                required: 'El mensaje es requerido',
                minlength: 'El mensaje debe ser más largo'
            }
        }
    });
});